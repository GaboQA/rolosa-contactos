import React, { useState, useEffect } from "react";
//import "bootstrap/dist/css/bootstrap.min.css";
import { db } from "../firebase";
import { toast } from "react-toastify";
import NumericInput from 'react-numeric-input';




const LinkForm = (props) => {
  
  const initialStateValues = {
    nombre: "",
    email: "",
    telefono: "",
    edad: "",
    provincia: "",
    genero: "",
    cedula: "",
  };

  const [values, setValues] = useState(initialStateValues);
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });

    
                      /*//Extraemos cedula en forma de string y convertimos en integer ademas de eliminar guiones
                      var texto = values.cedula;
                      var part1 = texto.slice(0,2);
                      var part2 = texto.slice(3,7);
                      var part3 = texto.slice(8,12);
                      var total = part1+part2+part3;
                      Cedula = Number(total);
                      console.log(Cedula+"imprimo cedula B");
                      values.cedula = Cedula;
                      console.log(values.cedula+"imprimo cedula ");*/
                      console.log(values.cedula);
                      values.cedula = 207910712;
  };

  const handleSubmit = (e) => {
    
    if(values.cedula.length == 9){
      var gui = '0'+values.cedula;
      var guion = '-';
      var result = gui.replace(/^(.{2})(.*)$/, '$1' + guion + '$2');
      var result2 = result.replace(/^(.{7})(.*)$/, '$1' + guion + '$2');
      values.cedula = result2;
    }
    
    e.preventDefault();
    props.addOrEditLink(values);
    setValues({ ...initialStateValues });
  };
  const getLinkById = async (id) => {
    const doc = await db.collection("links").doc(id).get();
    //console.log(values.cedula);
    setValues({ ...doc.data() });
  };
  var Cedula;
  useEffect(() => {
    if (props.currentId === "") {

      setValues({ ...initialStateValues });

    } else {

      getLinkById(props.currentId);
      console.log(props.currentId);

    }
    
  }, [props.currentId]);


  //Ajustamo lon guiones en la cedula

  //console.log(result2);
/*
    if(values.cedula > 999999999 && values.cedula < 99999999){
      toast("Falta completar la cédula", {
        type: "success",
        autoClose: 2000,
      });
      values.cedula = '';
    }

*/

//Extraemos cedula en forma de string y convertimos en integer ademas de eliminar guiones
if(values.cedula.length > 999999999 && values.cedula.length < 99999999){
  var texto = values.cedula;
  var part1 = texto.slice(0,2);
  var part2 = texto.slice(3,7);
  var part3 = texto.slice(8,12);
  var total = part1+part2+part3;
  Cedula = Number(total);
  console.log(Cedula+"imprimo cedula B");
  values.cedula = Cedula;
 
}




  return (    
    <form className="card card-body" onSubmit={handleSubmit}>
 <div className="form-group input-group">

        

        <input
          type="text"
          className="form-control"
          placeholder="|  Nombre"
          name="nombre"
          onChange={handleInputChange}
          value={values.nombre}
        ></input>

      </div>
      <div className="form-group input-group">
        <input
          type="email"
          id="email"
          name="email"
          className="form-control"
          placeholder="|  Correo Electronico"
          onChange={handleInputChange}
          value={values.email}
        ></input>
      </div>

      <div className="form-group input-group">
        <input
          type="number"
          id="telefono"
          name="telefono"
          className="form-control"
          placeholder="|  Teléfono"
          onChange={handleInputChange}
          value={values.telefono}
        ></input>
      </div>

      <div className="form-group input-group">
        <input
          type="number"
          id="edad"
          name="edad"
          className="form-control"
          placeholder="|  Edad"
          onChange={handleInputChange}
          value={values.edad}
        ></input>
      </div>

      <div className="form-group">
      <select name="provincia"
          rows="7"
          className="form-control"
          placeholder="Provincia"
          onChange={handleInputChange}
          value={values.provincia}>
        <option value="Alajuela">Alajuela</option>
        <option value="San Jose">San José</option>
        <option value="Heredia">Heredia</option>
        <option value="Limon">Limón</option>
        <option value="Cartago">Cartago</option>
        <option value="Guanacaste">Guanacaste</option>
        <option value="Puntarenas">Puntarenas</option>
      </select>
      </div>

      <div className="form-group input-group">
      <label for="male">Género: </label>
        <input
          type="radio"
          id="genero"
          name="genero"
          className="form-control"
          placeholder="|  Teléfono"
          onChange={handleInputChange}
          value="M"
        />
        <label for="male">Mas</label><br></br>
          <input
          type="radio"
          id="genero"
          name="genero"
          className="form-control"
          placeholder="|  Teléfono"
          onChange={handleInputChange}
          value="F"
        ></input>
        <label for="male">Fem</label><br></br>
      </div>

      <div className="form-group input-group">
      <input
          maxLength='12'
          minLength="9"
          type="text"
          id="cedula"
          name="cedula"
          className="form-control"
          placeholder="|  Cédula"
          onChange={handleInputChange}
          value={values.cedula}
        ></input>
      </div>
  
      
      <button className="btn btn-primary btn-block">
        {props.currentId === "" ? "Guardar" : "Actualizar"}
      </button>
    </form>
  );
};
export default LinkForm;
/*<input type="text" name="nombre" style="width: 12.3em" />  E-mail: <input type="email" name="email" style="width:13.05em " onchange="return ValidarEmail(this)" /> 
     Teléfono: <input type="number" name="tel" style="width: 11.95em" onchange={ return ValidarCedula(this)} />
     
     <input type="number" min="1" maxLength="7" defaultValue={6}  />
     */