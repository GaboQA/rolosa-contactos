import React from "react";
import "../App.css";
import * as ReactBootStrap from "react-bootstrap";
import Links from "./Links";  
import Start from "./Start";  

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink 
} from "react-router-dom";

function Navbar({handleLogout}) {
  return (
    <Router>
    <section className="Navbar">
    <div className="Appnav">
      <ReactBootStrap.Navbar bg="light" expand="lg">
        <ReactBootStrap.Navbar.Brand>
          Administrador de Contactos
        </ReactBootStrap.Navbar.Brand>
        <ReactBootStrap.Navbar.Toggle aria-controls="basic-navbar-nav" />
        <ReactBootStrap.Navbar.Collapse id="basic-navbar-nav">
          <ReactBootStrap.Nav className="mr-auto">
          </ReactBootStrap.Nav>
          <ReactBootStrap.Form inline>            <Link className="navlinks" onClick={handleLogout} to="/">
              Cerrar sesión 
            </Link> </ReactBootStrap.Form>
        </ReactBootStrap.Navbar.Collapse>
      </ReactBootStrap.Navbar>
    </div>
    <Switch>
    <Route path="/" exact>
     <Start />
    </Route>
    <Route path="/Links" exact>
     <Links />
    </Route>
    </Switch>
    </section>
    </Router>
  );
}

export default Navbar;