import React, { useEffect, useState } from "react";
import LinkForm from "./LinkForm";
import { toast } from "react-toastify";
import { db } from "../firebase";


const Start = () => {
  const [links, setLinks] = useState([]);
  const [currentId, setCurrentId] = useState("");

  const addOrEditLink = async (LinkObject) => {
    try {
      if (currentId === "") {
        await db.collection("links").doc().set(LinkObject);
        toast("Nuevo contacto añadido", {
          type: "success",
          autoClose: 2000,
        });
      } else {
        await db.collection("links").doc(currentId).update(LinkObject);
        toast("Contacto Actualizado", {
          type: "info",
          autoClose: 2000,
        });
        setCurrentId("");
      }
    } catch (error) {
      console.error(error);
    }
  };
  const onDeleteLink = async (id) => {
    if (window.confirm("Estas seguro de eliminar este producto")) {
      await db.collection("links").doc(id).delete();
      console.log("eliminado");
      toast("Contacto Eliminado", {
        type: "error",
        autoClose: 2000,
      });
    }
  };
  const getLinks = async () => {
    db.collection("links").onSnapshot((querySnapshot) => {
      const docs = [];
      querySnapshot.forEach((doc) => {
        docs.push({ ...doc.data(), id: doc.id });
      });
      setLinks(docs);
    });
  };

  useEffect(() => {
    getLinks();
  }, []);


  return (
  <section className="Start">
  <body>
    <div id='main'>
      <nav>      
      <div className="dfimg">
          <center className="a p-4">
            <img className="imglogin" src="http://blog.aulaformativa.com/wp-content/uploads/2016/08/consideraciones-mejorar-primera-experiencia-de-usuario-aplicaciones-web-perfil-usuario.jpg" />
          </center>
      </div>
      <div className="dfimg">
          <center className="a p-4">
            <div className="contimg"> 
              <ol>
              <li>Informacion basica</li>
              <li class="m-1">Vista previa</li>
              <lo>Gestiones Permitidas</lo>
              <li>Agregar</li>
              <li>Modificar</li>
              <li>Eliminar</li>
              </ol>
            </div>
          </center>
      </div>
      </nav>
      <article>
        <center><h3>Contactos</h3></center>
      <div>
      <table>
      <center>
        <h4>Nombre</h4>
      </center>
      <tr>
        
        <center>
        {links.map((link) => (
          <div className="card mb-1" key={link.id}>
            <div className="card-body">
              <div className="d-flex justify-content-between">
                <h4>{link.nombre}</h4>
                <div>
                  <i
                    className="material-icons text-danger m-3"
                    onClick={() => onDeleteLink(link.id)}
                  >
                    delete
                  </i>
                  <li
                    className="material-icons"
                    onClick={() => setCurrentId(link.id)}
                  >
                    create
                  </li>
                </div>
              </div>
            </div>
          </div>
        ))}</center>
      </tr>

 
</table>
      <div className="col-md-8 p-2">

      </div>
    </div>

      </article>
      
      <aside><center><h3>Formulario</h3></center>
      <div className="col">
        <LinkForm {...{ addOrEditLink, currentId, links }} />
      </div>
      </aside>
      </div>
    <footer>
      <center>Prueba Técnica</center>
    </footer>
  </body>
  </section>
    )
};
export default Start;
