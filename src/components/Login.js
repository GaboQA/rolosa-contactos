import React from 'react';
const Login = (props) => {

    const {
        email,
        setEmail,
        password,
        setPassword,
        handleLogin,
        handleSignup,
        hasAccount,
        setHasAccount,
        emailError,
        passwordError,
    } = props;

    return(
        <section className="login">
             <div className="loginContainer">
             
                
                    {hasAccount ? (
                        <>
                        <h1 class="form__heading p-2">Registro de Usuario</h1>
                        <input type="text" autoFocus required value={email} onChange={(e) => setEmail (e.target.value)} placeholder="Usuario"/>
                        <p className="errorMsg">{emailError}</p>
                        <input type="password" required value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Contraseña"/>
                        <p className="errorMsg">{passwordError}</p>
                        <div className="contenedorF">
                        <div className="item">
                        <button className="btnLogin" onClick={handleSignup}>Registrar</button>
                        </div>
                        <div className="item">
                        <button className="btnLogin" onClick={()=> setHasAccount(!hasAccount)}>Regresar</button>
                        </div>
                        </div>

                        </>
                    ) : (
                        <>
                        <h1 class="form__heading p-2">Inicio de Sesion</h1>
                        <input type="text" autoFocus required value={email} onChange={(e) => setEmail (e.target.value)} placeholder="Usuario"/>
                        <p className="errorMsg">{emailError}</p>
                        <input type="password" required value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Contraseña"/>
                        <p className="errorMsg">{passwordError}</p>
                        <div className="contenedorF">
                        <div className="item">
                        <button className="btnLogin" onClick={handleLogin} >Acceder</button>
                        </div>
                        <div className="item">
                        <button className="btnLogin" onClick={() => setHasAccount(!hasAccount)}>Registro</button>
                        </div>

                        </div>
                        </>
                    )}
                
             </div>
        </section>
    );
};

export default Login;

/**
 * 
 */