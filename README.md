# Rolosa Mantenimiento de Contactos
Como primer paso debemos tener las siguiente herramientas para poder ejecutar dicha aplicación
## Número 1: npm instalado 
En este enlace podran instalar dicha herramienta.
https://nodejs.org/en/download/
### Número 2: realizar los siguientes comandos
npm install
npm update
---------------------------------
En caso de que falle instalamos 
las siguientes dependencias
---------------------------------
npm i firebase
npm install react-router-dom
npm install anychart-react
npm add react-firebaseui
npm i bootswatch        
npm install react-bootstrap bootstrap
#### Por ultimo corremos la aplicación
npm start

![alt text](https://gitlab.com/GaboQA/rolosa-contactos/-/raw/master/lg.png)
![alt text](https://gitlab.com/GaboQA/rolosa-contactos/-/raw/master/CRD.png)
![alt text](https://gitlab.com/GaboQA/rolosa-contactos/-/raw/master/rg.png)


