import React, { useEffect, useState } from "react";
import LinkForm from "./LinkForm";
import { toast } from "react-toastify";
import { db } from "../firebase";

const Links = () => {
  const [links, setLinks] = useState([]);
  const [currentId, setCurrentId] = useState("");

  const addOrEditLink = async (LinkObject) => {
    try {
      if (currentId === "") {
        await db.collection("links").doc().set(LinkObject);
        toast("Nuevo elementro añadido", {
          type: "success",
          autoClose: 2000,
        });
      } else {
        await db.collection("links").doc(currentId).update(LinkObject);
        toast("Usuario Actualizado", {
          type: "info",
          autoClose: 2000,
        });
        setCurrentId("");
      }
    } catch (error) {
      console.error(error);
    }
  };
  const onDeleteLink = async (id) => {
    if (window.confirm("Estas seguro de eliminar este producto")) {
      await db.collection("links").doc(id).delete();
      console.log("eliminado");
      toast("Producto Eliminado", {
        type: "error",
        autoClose: 2000,
      });
    }
  };
  const getLinks = async () => {
    db.collection("links").onSnapshot((querySnapshot) => {
      const docs = [];
      querySnapshot.forEach((doc) => {
        docs.push({ ...doc.data(), id: doc.id, cedula: doc.cedula});
      });
      setLinks(docs);
    });
  };

  useEffect(() => {
    getLinks();
  }, []);

  return (
    <div>
      <div className="col-md-4 p-2">
        <LinkForm {...{ addOrEditLink, currentId, links }} />
      </div>
      <div className="col-md-8 p-2">
        {links.map((link) => (
          <div className="card mb-1" key={link.id}>
            <div className="card-body">
              <div className="d-flex justify-content-between">
                <h4>{link.nombre}</h4>
                <div>
                  <i
                    className="material-icons text-danger"
                    onClick={() => onDeleteLink(link.id)}
                  >
                    delete
                  </i>
                  <li
                    className="material-icons"
                    onClick={() => setCurrentId(link.id)}
                  >
                    create
                  </li>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Links;
